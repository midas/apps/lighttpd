# Midas app: Lighttpd

A simple web server using [Lighttpd](https://lighttpd.net)

Can deploy static website in almost one click

This repository is part of the project **Midas** (for Minimalist Docker/Alpine Server). More infos in the [project wiki](https://gitlab.ensimag.fr/groups/midas/-/wikis/home).