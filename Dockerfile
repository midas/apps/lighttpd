# Image to build app from
FROM alpine:latest

RUN apk add --update --no-cache lighttpd \
    && rm -rf /var/cache/apk/*

CMD ["lighttpd","-D","-f","/etc/lighttpd/lighttpd.conf"]
